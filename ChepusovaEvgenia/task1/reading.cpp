// ***** generation.cpp *****

#include <ctime> // time_t
#include <cstdio> // sprintf, fprintf
#include <iostream> //cin cout
#include <cstdlib> // exit success
#include "source.hpp"
#include "sqlite3.h"
using namespace std;

int main (int argc, char* argv[])
{
    cout <<"\nПрограмма чтения показаний датчиков температуры и влажности "
    "из базы данных.\n";
    sqlite3 *db = 0; // хэндл объекта соединение к БД
    char *err = 0;
    // открываем соединение 
    if( sqlite3_open("sensors.db", &db) )
        fprintf(stderr,"Ошибка открытия/создания БД: %s\n", sqlite3_errmsg(db));
    
    while(true)
    {
        cout << "\nСписок датчиков в базе данных: \n";
        SensorData request; // используем как временное хранилище 
        int maxId = request.getAllSensors(db);
        int selection; // номер датчика
        while(true)
        {
            cout << "Выберите номер датчика (0-для выхода): ";
            cin >> selection;
            if(selection < 0 || selection > maxId)
                cout << "Выберите снова. \n";
            else
                break;
        }
        if(!selection)
        {
            cout << "Чтение данных из базы закончено.\n";
            break;
        }
        request.getSelectionSensor(db, selection);
        time_t begin, end; // диапазон времени измерений
        while(true)
        {
            cout << "Выберите период измерений: \n";
            cout << "Выбор даты начала периода \n";
            begin = request.getTimeRange(db, selection);
            cout << "Выбор даты конца периода \n";
            end = request.getTimeRange(db, selection);
            if(begin > end)
                cout <<"Дата начала периода позже даты окончания.\n";
            else
                break;
        }
        
        while(true)
        {
            int choice = 0; // выбор задачи для обрадотки данных
            while(true)
            {
                cout << "Выберите результат обработки данных: \n";
                cout << "1 - минимальное значение;\n";
                cout << "2 - среднее значение;\n";
                cout << "3 - максимальное значение;\n";
                cout << "4 - три предыдущих параметра;\n";
                cout << "0 - вернуться к списку датчиков.\n\n";
                cin >> choice;
                if(choice < 0 || choice > 4)
                    cout << "Выберите снова. \n";
                else
                    break;
            }
            if(!choice)
                break;
            else
            {
                request.getInfo(); // информация о датчике
                switch(choice)
                {
                    case 1:
                        request.getMinReading(db, selection, begin, end);
                        break;
                    case 2:
                        request.getMiddleReading(db, selection, begin, end);
                        break;
                    case 3:
                        request.getMaxReading(db, selection, begin, end);
                        break;
                    case 4:
                        request.getMinReading(db, selection, begin, end);
                        request.getMiddleReading(db, selection, begin, end);
                        request.getMaxReading(db, selection, begin, end);
                        break;
                }
            }
        }
    }
    sqlite3_close(db); // закрытие соединения с БД
    return EXIT_SUCCESS;
}