// *****source.hpp

#ifndef SOURCE_HPP
#define SOURCE_HPP
#include <ctime> // time_t
#include <vector> // vector
#include <string> // string
#include "sqlite3.h"
using namespace std;

class Measures
{
public:
    Measures();
    Measures(time_t timeAndDate, double temperatureMeasure,
    double humidityMeasure);
    void getMeasures();
    void entryDBMeasures(sqlite3 *db, int numberSensor);
    time_t dateAndTime;
    double temperature;
    double humidity;
};

class Sensor
{
public:
    Sensor();
    Sensor(const char *str, time_t timeAndDate, 
    int latLocation, int longLocation);
    void setAll(const char *str, time_t timeAndDate, 
    int latLocation, int longLocation);
    void setModel();
    void setCoordinates();
    time_t conversionTime();
    void setDate();
    void getInfo();
    char *getModel();
    int getLatitude();
    int getLongitude();
    time_t getInstallDate();
private:
    string model; // модель
    int latitude; // место установки широта, секунды
    int longitude; // место установки долгота, секунды
    time_t installationDate; // дата установки
};

class SensorData : public Sensor
{
public:
    SensorData(){};
    SensorData(const char *str, time_t timeAndDate, int latLocation,
    int longLocation) : Sensor(str, timeAndDate, latLocation, longLocation){};
    void generateMesuares(sqlite3 *db);
    void databaseEntry(sqlite3 *db);
    int getAllSensors(sqlite3 *db);
    void getSelectionSensor(sqlite3 *db, int id);
    time_t getTimeRange(sqlite3 *db, int id);
    
    void getMaxReading(sqlite3 *db, int id, time_t timeBegin, time_t timeEnd);
    void getMinReading(sqlite3 *db, int id, time_t timeBegin, time_t timeEnd);
    void getMiddleReading(sqlite3 *db,int id, time_t timeBegin, time_t timeEnd);
};
#endif // SOURCE_HPP