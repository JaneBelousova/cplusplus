// *****source.cpp

#include <vector> // vector
#include <iostream> // cin, cout
#include <iomanip> // << setup
#include <string> // string
#include <cstdlib> // srand, rand
#include <cstring> // strlen
#include <ctime> // struct tm, time_t, mktime, time, localtime, ctime
#include "source.hpp"
#include "sqlite3.h"
using namespace std;

ostream &setup(ostream &stream)
{
    stream << setw(2) << setfill('0');
    return stream;
}

struct Coordinate
{
    Coordinate(int form); // form=0 (широта), form!=0 (долгота),
    int type; // тип координаты (широта=0, долгота=1)
    int partOfWorld; // (+)N (-)S (+)E (-)W
    int degrees; // градусы
    int minutes; // минуты
    int seconds; // секунды
    void set();
    int conversion();
};

// struct Coordinate
Coordinate::Coordinate(int form)
{
    type=form;
}

void Coordinate::set()
{
    cout << "Введите координаты установки датчика: \n";
    // (+)N (-)S (+)E (-)W
    char part = '-';
    partOfWorld = 0;
    if(type==0)
    {
        while(!partOfWorld)
        {
            cout << "широта (N - северная, S - южная) ";
            cin >> part;
            if(part=='N')
                partOfWorld = 1;
            else if(part=='S')
                partOfWorld = -1;
            else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
        }
    }
    else
    {
        while(!partOfWorld)
        {
            cout << "долгота (E - восточная, W - западная) ";
            cin >> part;
            if(part=='E')
                partOfWorld = 1;
            else if(part=='W')
                partOfWorld = -1;
            else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
        }
    }
    degrees = -1; // градусы
    if(type==0)
    {
        while(degrees < 0)
        {
            cout << "градусы (от 0 до 90) ";
            cin >> degrees;
            if(degrees<0 || degrees>90)
            {
                cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
                degrees = -1;
            }
        }
    }
    else
    {
        while(degrees < 0)
        {
            cout << "градусы (от 0 до 180) ";
            cin >> degrees;
            if(degrees<0 || degrees>180)
            {
                cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
                degrees = -1;
            }
        }
    }
    minutes = -1; // минуты
    while(minutes < 0)
    {
        cout << "минуты (от 0 до 59) ";
        cin >> minutes;
        if(minutes<0 || minutes>59)
        {
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
            minutes = -1;
        }
    }
    seconds = -1; // минуты
    while(seconds < 0)
    {
        cout << "секунды (от 0 до 59) ";
        cin >> seconds;
        if(seconds<0 || seconds>59)
        {
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
            seconds = -1;
        }
    }
}

int Coordinate::conversion()
{
    return partOfWorld * (3600*degrees + 60*minutes + seconds);
}

// class Measures
Measures::Measures()
{
    dateAndTime=0;
    temperature = 0;
    humidity = 0;
}
Measures::Measures(time_t timeAndDate, double temperatureMeasure, 
double humidityMeasure)
{
    dateAndTime = timeAndDate; // секунды
    //cout << ctime(&dateAndTime);
    temperature = temperatureMeasure;
    //cout << temperature << "\t";
    humidity = humidityMeasure;
    //cout << humidity << "\n";
}

void Measures::getMeasures()
{
    cout << "дата и время измерения " << ctime(&dateAndTime);
    cout << "температура " << temperature << " °C,\n";
    cout << "влажность " << humidity << "%\n\n";
}

void Measures::entryDBMeasures(sqlite3 *db, int numberSensor)
{
    //cout << "entryDBMeasures\n";
    const char * query1 = "CREATE TABLE IF NOT EXISTS measures_%d(Id integer "
    "primary key autoincrement, DateAndTime integer, Temperature real, "
    "Humidity real);";
    const char * query2 = "INSERT INTO measures_%d VALUES(NULL, ?, ?, ?);";
    int size1 = strlen(query1);
    char *SQL1 = new char [size1+11];
    int size2 = strlen(query2);
    char *SQL2 = new char [size2+11];
    sprintf (SQL1, query1, numberSensor);
    sprintf (SQL2, query2, numberSensor);
    sqlite3_stmt *stmt;
    int rc;
    getMeasures();
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    if(rc)
        fprintf(stderr, "Ошибка SQL: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    sqlite3_reset(stmt);
    rc = sqlite3_prepare_v2(db, SQL2, -1, &stmt, 0);
    sqlite3_bind_int64(stmt, 1, dateAndTime);
    sqlite3_bind_double(stmt, 2, temperature);
    sqlite3_bind_double(stmt, 3, humidity);
    //cout<< "rc="<<rc<<endl;
    if(rc)
        fprintf(stderr, "Ошибка SQL: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    //cout << "entryDBMeasures-end\n";
}

// class Sensor
Sensor::Sensor()
{
    model="-";
    latitude = longitude = 0;
    installationDate = 0;
}

Sensor::Sensor(const char *str, time_t timeAndDate, 
int latLocation, int longLocation)
{
    model = str;
    latitude = latLocation; // секунды
    longitude = longLocation; // секунды
    installationDate = timeAndDate; // секунды
}

void Sensor::setAll(const char *str, time_t timeAndDate, 
int latLocation, int longLocation)
{
    model = str;
    latitude = latLocation; // секунды
    longitude = longLocation; // секунды
    installationDate = timeAndDate; // секунды
    //cout<<"setAll-end"<<endl;
}

void Sensor::setModel()
{
    cout << "Введите модель датчика температуры и влажности: ";
    cin >> model;
}

void Sensor::setCoordinates()
{
    Coordinate locationLatitude(0); //широта
    locationLatitude.set();
    latitude = locationLatitude.conversion();
    Coordinate locationLongitude(1); //долгота
    locationLongitude.set();
    longitude = locationLongitude.conversion();
}

time_t Sensor::conversionTime()
{
    struct tm* currentTime;
    time_t timeNow = time(NULL);
    currentTime = localtime(&timeNow);
    struct tm time;
    cout << "Введите дату и время: \n";
    while(true)
    {
        cout << "год (от 1970 до настоящего времени "
        << 1900+currentTime->tm_year << ") ";
        cin >> time.tm_year;
        if(time.tm_year>=1970 && time.tm_year<=1900+currentTime->tm_year)
        {
            time.tm_year -= 1900; // год
            break;
        }
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    while(true)
    {
        cout << "месяц (от 1=январь до 12=декабрь) ";
        cin >> time.tm_mon;
        if(time.tm_mon>=1 && time.tm_mon<=12)
        {
            time.tm_mon -= 1; // месяц
            break;
        }
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    while(true)
    {
        cout << "день (от 1 до 31) ";
        cin >> time.tm_mday; // день
        if(time.tm_mday>=1 && time.tm_mday<=28)
            break;
        else if((time.tm_mday==29 && time.tm_mon!=1)
        ||(time.tm_mday==29 && time.tm_mon==1 && (time.tm_year+1900)%4==0))
            break;
        else if(time.tm_mday==30 && time.tm_mon!=1)
            break;
        else if(time.tm_mday==31 && time.tm_mon!=1 && time.tm_mon!=3 
        && time.tm_mon!=5 && time.tm_mon!=8 && time.tm_mon!=10)
            break;
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    while(true)
    {
        cout << "часы (от 0 до 23) ";
        cin >> time.tm_hour;
        if(time.tm_hour>=0 && time.tm_hour<=23)
            break; // часы
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    while(true)
    {
        cout << "минуты (от 0 до 59) ";
        cin >> time.tm_min;
        if(time.tm_min>=0 && time.tm_min<=59)
            break; // часы
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    while(true)
    {
        cout << "секунды (от 0 до 59) ";
        cin >> time.tm_sec;
        if(time.tm_sec>=0 && time.tm_sec<=59)
            break; // часы
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    cout << endl;
    return mktime(&time);
}

void Sensor::setDate()
{
    time_t now = time(NULL);
    while(true)
    {
        cout <<"Внесите данные об установке датчика.\n";
        time_t installation = conversionTime();
        if(installation<=now)
        {
            installationDate = installation;
            break;
        }
        else
            cout <<"Дата и время из будущего. Введите данные ещё раз.\n";
    }
}

void Sensor::getInfo()
{
    cout << "модель " << model << ",\n";
    cout << "место установки: широта ";
    if(latitude>=0)
        cout<<"\"N\" - " << setup << latitude/3600 << "° "
        << setup << (latitude%3600)/60 << "\' " << setup << (latitude%3600)%60
        << "\" долгота ";
    else
        cout<<"\"S\" - " << setup << -latitude/3600 << "° " << setup
        << -(latitude%3600)/60 << "\' " << setup << -(latitude%3600)%60
        << "\" долгота ";
    if(longitude>=0)
        cout <<  "\"E\" - " << setup << longitude/3600 << "° "
        << setup << (longitude%3600)/60 << "\' " << setup << (longitude%3600)%60
        << "\" ,\n";
    else
        cout<<"\"W\" - " << setup << -longitude/3600 << "° " << setup
        << -(longitude%3600)/60 << "\' " << setup << -(longitude%3600)%60
        << "\" ,\n";
    cout << "дата установки " << ctime(&installationDate) << "\n";
}

char *Sensor::getModel()
{
    int n = model.size();
    char *str = new char[n+1];
    for(int i =0; i < n; ++i)
        str[i] = model[i];
    str[n] = '\0';
    return str;
}

int Sensor::getLatitude()
{
    return latitude;
}

int Sensor::getLongitude()
{
    return longitude;
}

time_t Sensor::getInstallDate()
{
    return installationDate;
}

void SensorData::generateMesuares(sqlite3 *db)
{
    //cout << "generateMesuares\n";
    time_t period=0; // периодичность получения замеров
    while(true)
    {
        cout << "Установите периодичность обновления данных в секундах " <<
        "(1мин.=60с., 1ч. = 3600с., 1дн.=86400с.): от 1с. до 7дн.\n";
        cin >> period;
        if(period >= 1 && period <= 86400*7)
            break;
        else
            cout <<"Недопустимое значение. Попробуйте ещё раз.\n";
    }
    time_t now = time(NULL);
    time_t installation = getInstallDate();
    srand (now);
    const char* SQL2 = "SELECT MAX(Id) FROM sensors;";
    sqlite3_stmt *stmt;
    int rc, id;
    rc = sqlite3_prepare_v2(db, SQL2, -1, &stmt, 0);
    //cout<< "rc="<<rc<<endl;
    if(rc)
        fprintf(stderr, "Ошибка SQL2: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    id = sqlite3_column_int( stmt, 0);
    //cout << id<<" ID\n";
    sqlite3_finalize(stmt);
    for(time_t recordingTime = 0; recordingTime <= now-installation; 
    recordingTime += period)
    {
        double temperatureRand = -40 + rand() % 80 + 0.1 * (rand() % 10);
        double humidityRand = 30+ rand() % 70 + 0.1 * (rand() % 10);
        Measures *p_measure = new Measures(installation + recordingTime, 
        temperatureRand, humidityRand);
        p_measure -> entryDBMeasures(db, id);
        delete p_measure;
    }
    //cout << "generateMesuares-end\n";
}

void SensorData::databaseEntry(sqlite3 *db)
{
    const char* SQL = "CREATE TABLE IF NOT EXISTS sensors(Id integer primary "
    "key autoincrement, Model text, Latitude integer, Longitude integer, "
    "Installation integer);";
    const char* SQL1 = "INSERT INTO sensors VALUES(NULL, ?, ?, ?, ?);";
    int rc;
    sqlite3_stmt *stmt;
    //cout << "databaseEntry\n";
    rc = sqlite3_prepare_v2(db, SQL, -1, &stmt, 0);
    if(rc)
        fprintf(stderr, "Ошибка SQL: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    sqlite3_reset(stmt);
    //getInfo();
    //cout << getModel()<<"\t"<< getLatitude()<<"\t"<< getLongitude()<<"\t"
    //<< getInstallDate()<<endl;
    
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    sqlite3_bind_text(stmt, 1, getModel(), -1, 0);
    sqlite3_bind_int(stmt, 2, getLatitude());
    sqlite3_bind_int(stmt, 3, getLongitude());
    sqlite3_bind_int64(stmt, 4, getInstallDate());
    //cout<< "rc="<<rc<<endl;
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    //cout << "databaseEntry-end\n";
}

int SensorData::getAllSensors(sqlite3 *db)
{
    const char* SQL1 = "SELECT * FROM sensors;";
    int rc, id;
    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    while(sqlite3_step(stmt) == SQLITE_ROW)
    {
        id = sqlite3_column_int( stmt, 0);
        setAll((char *)sqlite3_column_text(stmt, 1), 
        sqlite3_column_int64(stmt, 4), 
        sqlite3_column_int(stmt, 2), 
        sqlite3_column_int(stmt, 3));
        cout << "№" << id << endl;
        getInfo();
    }
    sqlite3_finalize(stmt);
    return id;
}

void SensorData::getSelectionSensor(sqlite3 *db, int id)
{
    const char* SQL1 = "SELECT * FROM sensors WHERE Id = ?;";
    //cout << SQL1<<endl;
    //cout << id<<endl;
    int rc;
    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    //cout << rc<<endl;
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    setAll((char *)sqlite3_column_text(stmt, 1), 
        sqlite3_column_int64(stmt, 4), 
        sqlite3_column_int(stmt, 2), 
        sqlite3_column_int(stmt, 3));
    cout << "№" << id << endl;
    //getInfo();
    sqlite3_finalize(stmt);
}

time_t SensorData::getTimeRange(sqlite3 *db, int id)
{
    time_t installation = getInstallDate();
    cout << "Дата и время установки датчика (первого измерения): "
    << ctime(&installation) << "\n";
    const char* query1 = "SELECT MAX(Id) FROM measures_;";
    int size1 = strlen(query1);
    char *SQL1 = new char [size1+11];
    sprintf (SQL1, "SELECT MAX(Id) FROM measures_%d;", id);
    //cout << SQL1<<endl;
    sqlite3_stmt *stmt;
    int rc, idMaxMeasures;
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    idMaxMeasures = sqlite3_column_int(stmt, 0);
    //cout << idMaxMeasures<<endl;
    sqlite3_reset(stmt);
    const char* query2 = "SELECT * FROM measures_ WHERE Id = ?;";
    int size2 = strlen(query2);
    char *SQL2 = new char [size2+11];
    sprintf (SQL2, "SELECT DateAndTime FROM measures_%d WHERE Id = ?;", id);
    //cout << SQL2<<endl;
    rc = sqlite3_prepare_v2(db, SQL2, -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, idMaxMeasures);
    if(rc)
        fprintf(stderr, "Ошибка SQL2: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    time_t endTime = sqlite3_column_int64(stmt, 0);
    //cout << endTime<<endl;
    sqlite3_finalize(stmt);
    cout << "Дата и время последнего измерения: " << ctime(&endTime) << "\n";
    time_t choice;
    while(true)
    {
    cout << "Укажите дату и время из показанного диапазона.\n";
    choice = conversionTime();
    if(choice >= installation && choice <= endTime)
        break;
    }
    return choice;
}

void SensorData::getMaxReading(sqlite3 *db, int id, time_t timeBegin, 
time_t timeEnd)
{
    const char* query1 = "SELECT COUNT(*), MAX(Temperature), MAX(Humidity) "
    "FROM measures_%d WHERE DateAndTime>=? AND DateAndTime<=?;";
    int size1 = strlen(query1);
    char *SQL1 = new char [size1+11];
    sprintf (SQL1, query1, id);
    sqlite3_stmt *stmt;
    int rc, count;
    double maxTemperature, maxHumidity;
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    sqlite3_bind_int64(stmt, 1, timeBegin);
    sqlite3_bind_int64(stmt, 2, timeEnd);
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    count = sqlite3_column_int(stmt, 0);
    maxTemperature = sqlite3_column_double(stmt, 1);
    maxHumidity = sqlite3_column_double(stmt, 2);
    sqlite3_finalize(stmt);
    if(!count)
        cout << "во введенном диапазоне нет измерений\n\n";
    else
        cout << "Максимальное значение температуры "<< maxTemperature << "гр.\n"
        << "Максимальное значение влажности " << maxHumidity << "%\n\n"; 
}

void SensorData::getMinReading(sqlite3 *db, int id, time_t timeBegin, 
time_t timeEnd)
{
    const char* query1 = "SELECT COUNT(*), MIN(Temperature), MIN(Humidity) "
    "FROM measures_%d WHERE DateAndTime>=? AND DateAndTime<=?;";
    int size1 = strlen(query1);
    char *SQL1 = new char [size1+11];
    sprintf (SQL1, query1, id);
    sqlite3_stmt *stmt;
    int rc, count;
    double minTemperature, minHumidity;
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    sqlite3_bind_int64(stmt, 1, timeBegin);
    sqlite3_bind_int64(stmt, 2, timeEnd);
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    count = sqlite3_column_int(stmt, 0);
    minTemperature = sqlite3_column_double(stmt, 1);
    minHumidity = sqlite3_column_double(stmt, 2);
    sqlite3_finalize(stmt);
    if(!count)
        cout << "во введенном диапазоне нет измерений\n\n";
    else
        cout << "Минимальное значение температуры " << minTemperature << "гр.\n"
        << "Минимальное значение влажности " << minHumidity << "%\n\n";
}

void SensorData::getMiddleReading(sqlite3 *db, int id, time_t timeBegin, 
time_t timeEnd)
{
    const char* query1 = "SELECT COUNT(*), AVG(Temperature), AVG(Humidity) "
    "FROM measures_%d WHERE DateAndTime>=? AND DateAndTime<=?;";
    int size1 = strlen(query1);
    char *SQL1 = new char [size1+11];
    sprintf (SQL1, query1, id);
    sqlite3_stmt *stmt;
    int rc, count; 
    double avgTemperature, avgHumidity;
    rc = sqlite3_prepare_v2(db, SQL1, -1, &stmt, 0);
    sqlite3_bind_int64(stmt, 1, timeBegin);
    sqlite3_bind_int64(stmt, 2, timeEnd);
    if(rc)
        fprintf(stderr, "Ошибка SQL1: %s\n", sqlite3_errmsg(db));
    sqlite3_step(stmt);
    count = sqlite3_column_int(stmt, 0);
    avgTemperature = sqlite3_column_double(stmt, 1);
    avgHumidity = sqlite3_column_double(stmt, 2);
    sqlite3_finalize(stmt);
    if(!count)
        cout << "во введенном диапазоне нет измерений\n\n";
    else
        cout << "Среднее значение температуры " << avgTemperature
        <<"гр.\n"<< "Среднее значение влажности " << avgHumidity <<"%\n\n";
}