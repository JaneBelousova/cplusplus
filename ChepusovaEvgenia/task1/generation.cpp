// ***** generation.cpp *****

#include <ctime> // time_t
#include <cstdio> // sprintf, fprintf
#include <iostream> //cin cout
#include <cstdlib> // exit success
#include "source.hpp"
#include "sqlite3.h"
using namespace std;

int main (int argc, char* argv[])
{
    cout <<"\nПрограмма записи показаний датчиков температуры и влажности в"
    " базу данных.\n\n";
    cout << "Требуется указать модель, координаты "
    "установки и дату установки датчиков.\n";
    cout << "Укажите количество датчиков для внесения в базу данных, "
    "либо 0 для выхода.\n";
    int quantity = 0;
    while(true)
    {
        cin >> quantity;
        if(quantity > 0)
            break;
        else if(quantity == 0)
            return EXIT_SUCCESS;
        else
            cout <<"Ошибка! Ввведите положительное число, либо 0.\n";
    }
    sqlite3 *db = 0; // хэндл объекта соединение к БД
    char *err = 0;
    // открываем соединение 
    if( sqlite3_open("sensors.db", &db) )
        fprintf(stderr,"Ошибка открытия/создания БД: %s\n", sqlite3_errmsg(db));
    int complete = 0; // автозаполнение данных датчиков
    cout << "Требуется автозаполнение данных датчиков (модель, " 
    "координаты, дата установки)?\n0-да, 1(и др.числа)-нет: ";
    cin >> complete;
    if(!complete) // автозаполнение
    {
        for(int i=0; i<quantity; ++i)
        {
            char *pBuffer = new char [6];
            sprintf (pBuffer, "DHT%d", 11*(i%2+1));
            SensorData *ptr = new SensorData(pBuffer,time(NULL)-300000*(i%50+1),
            201600+3152*(i%5), 216000+3517*(i%8));
            ptr -> databaseEntry(db); //запись в таблицу сенсоров
            ptr -> generateMesuares(db); //запись в таблицу измерений
            delete [] pBuffer;
            delete ptr;
        }
    }
    else // заполнение вручную
    {
        for(int i=0; i<quantity; ++i)
        {
            SensorData *ptr = new SensorData;
            ptr -> setModel();
            ptr -> setCoordinates();
            ptr -> setDate();
            ptr -> databaseEntry(db); //запись в таблицу сенсоров
            ptr -> generateMesuares(db); //запись в таблицу измерений
            delete ptr;
        }
    }
    sqlite3_close(db); // закрываем соединение
    cout << "Внесение данных в базу закончено.\n\n";
    return EXIT_SUCCESS;
}